const dataFeatures = [
    {
        id: "feature-1",
        tab_title:'centralize',
        content_title:"Collect, store and organize all your ecommerce data in one place",
        content_text:"Organizing data and leveraging it to take decision has juste become easier with Dive. <br>Welcome to your new data hub that you will actually love using ❤️",
        url:"https://res.cloudinary.com/dad2k5sll/image/upload/v1630505042/centralize_feature_yjqxyb.png",
        alt: "dive-centralize-feature",
        width: 600,
        height: 405,
    },
    {
        id: "feature-2",
        tab_title:'analyze',
        content_title:"Take control of your data, not the other way round",
        content_text:"Dive is the easiest way to play with your data, in an environement you know best: spreadsheet. Live data, complexe pivot tables, in-depth analysis, beautiful dashboards... <br>Your new superpowers are just a cell away!️",
        url:"https://res.cloudinary.com/dad2k5sll/image/upload/v1630505284/analyze_feature_coyvz4.png",
        alt: "dive-analyze-feature",
        width: 600,
        height: 375,
    },
    {
        id: "feature-3",
        tab_title:'collaborate',
        content_title:"Make the bond between data and collaboration",
        content_text:"Work with your team around data: Share reports, run forecasts, set goals,  assign tasks...Simplify communication and make data selfserve for non-tech stakeholders with a new generation BI tool built for ecommerce.",
        url:"https://res.cloudinary.com/dad2k5sll/image/upload/v1630505284/collaborate_feature_o1vccu.png",
        alt: "dive-collaborate-feature",
        width: 530,
        height: 275,
    },
    
]

export default { dataFeatures }